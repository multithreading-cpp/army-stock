#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <string>

/*
������-������ ����� ���������� ������, ������ � ������� ���������� �������� �������� ���������
�� ������ ������ ������� �����. ������ ������ ������ � ����������� ������ � �������� ����������, 
���������� ����� ���������� �����: ������ ������� ��������� �� ������, ������ ������ ��� � ��������,
� ������� ������������ �������� ��������� ������. ��������� ��������� ������������� ����������,
������������ ������������ �����������. 
*/

using namespace std;

mutex lorry_mutex;
mutex stock_mutex;
mutex console_mutex;

condition_variable stock_cond_var;
condition_variable lorry_cond_var;

const unsigned int maxBufferSize = 10;

void putTextToConsole(string msg)
{
	console_mutex.lock();
	cout << msg << endl;
	console_mutex.unlock();
}

void Ivanov(queue<int> &stock, int &item_count)
{
	while (item_count)
	{
		unique_lock<mutex> stock_lock(stock_mutex);
		stock_cond_var.wait(stock_lock, [&]() { return stock.size() < maxBufferSize; });
		int cost = rand() % item_count;
		stock.push(cost);
		auto s = to_string(cost);
		string msg = "Ivanov took item worth " + s;
		putTextToConsole(msg);
		item_count--;
		stock_lock.unlock();
		stock_cond_var.notify_one();
	}
}

void Petrov(queue<int> &stock, queue<int> &lorry)
{
	while (true)
	{
		unique_lock<mutex> stock_lock(stock_mutex);
		stock_cond_var.wait(stock_lock, [&]() { return !stock.empty(); });
		int cost = stock.front();
		stock.pop();
		auto s = to_string(cost);
		string msg = "Petrov took from stock item worth " + s;
		putTextToConsole(msg);
		unique_lock<mutex> lorry_lock(lorry_mutex);
		stock_cond_var.wait(lorry_lock, [&]() { return lorry.size() < maxBufferSize; });
		lorry.push(cost);
		lorry_lock.unlock();
		lorry_cond_var.notify_one();
		stock_lock.unlock();
		stock_cond_var.notify_one();
	}
}

void Sidorov( queue<int> &lorry, int& total_cost)
{
	while (true)
	{
		unique_lock<mutex> lorry_lock(lorry_mutex);
		stock_cond_var.wait(lorry_lock, [&]() { return !lorry.empty(); });
		int cost = lorry.front();
		total_cost += cost;
		lorry.pop();
		auto s = to_string(total_cost);
		string msg = "Total cost by now " + s;
		putTextToConsole(msg);
		lorry_lock.unlock();
		stock_cond_var.notify_one();
	}
}

void main()
{
	queue<int> stock;

	queue<int> lorry;
	int total_cost = 0;
	int item_count = 12;
	thread ivanov([&]() {Ivanov(stock, item_count); });
	thread petrov([&]() {Petrov(stock, lorry); });
	thread sidorov([&]() {Sidorov(lorry, total_cost); });

	ivanov.join();
	petrov.join();
	sidorov.join();
}